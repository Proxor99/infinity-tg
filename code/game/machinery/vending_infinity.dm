/obj/machinery/vending/clothing/outerspace
	name = "\improper Outerspace-Clothes"
	desc = "Outerspace designer brand machine! For truly style!"
	icon = 'icons/obj/infinity_object.dmi'
	icon_state = "outerspace_clothe"
	product_slogans = "Stop right here! Outerspace designer here! ;It's time for new clothe-experience! ;What? Never see that before?"
	vend_reply = "Outerspace designer thanks you!"
	products = list(/obj/item/clothing/under/mai_yang=2,/obj/item/clothing/under/dress_pink=2,/obj/item/clothing/under/dress_green=2,
	/obj/item/clothing/under/sundress_white=3,/obj/item/clothing/under/dress_fire=3,/obj/item/clothing/under/dress_yellow=2,/obj/item/clothing/under/dress_saloon=1,
	/obj/item/clothing/under/schoolgirlblack=1,/obj/item/clothing/suit/brand/blue_jacket=2,/obj/item/clothing/suit/hooded/black_hoody=2,
	/obj/item/clothing/suit/toggle/jacket=2,/obj/item/clothing/suit/dress_orange=1,/obj/item/clothing/suit/bride_white=1,
	/obj/item/clothing/suit/brand/orange_jacket=2,/obj/item/clothing/under/red_cheongasm=2,/obj/item/clothing/shoes/ballets/red_gold=2,
	/obj/item/clothing/suit/toggle/fiery_jacket=2,/obj/item/clothing/suit/toggle/white_fiery_jacket=2,/obj/item/clothing/suit/leon_jacket=1,/obj/item/clothing/under/latex_pants=1,
	/obj/item/clothing/head/bowknot=1,/obj/item/clothing/under/kos_shorts=2,/obj/item/clothing/under/kos_bshorts=2,/obj/item/clothing/under/kos_bshorts2=2,/obj/item/clothing/under/womentshirt=1,
	/obj/item/clothing/suit/jacket/leather/overcoat=2, /obj/item/clothing/suit/longjacket=2)
